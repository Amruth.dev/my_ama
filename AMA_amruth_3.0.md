# Technical Paper

## 1. Express over Node.js

**Express** is a minimal and flexible Node.js web application framework. It provides robust features for web and mobile applications. Here's a simple example of setting up an Express application:

```javascript
// Install Express using npm
// npm install express

const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
```

## 2. What is a Subquery?

A **subquery** is a query within another query. They are used to perform operations in multiple steps. Here's a SQL example:

```sql
SELECT * FROM employees WHERE department_id IN (SELECT id FROM departments WHERE name = 'Sales');
```

## 3. Different Types of Events in Event Listeners

Event listeners in JavaScript listen for different types of events like:

- **Mouse Events**: click, mouseover, mouseout
- **Keyboard Events**: keydown, keyup, keypress
- **Form Events**: submit, change, focus
- **Window Events**: load, resize, scroll

Example:

```javascript
document.getElementById("myButton").addEventListener("click", () => {
  alert("Button clicked!");
});
```

## 4. DOM Tree and DOM Nodes

The **Document Object Model (DOM)** tree is a representation of the HTML document structure. Nodes in the DOM can be elements, attributes, text, etc.

Example:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>DOM Example</title>
  </head>
  <body>
    <div id="container">
      <p>This is a paragraph.</p>
    </div>
  </body>
</html>
```

In the above HTML, the DOM tree would include nodes for the <html>, <head>, <body>, <div>, and <p> elements, along with their text.

## 5. Event Capturing and Event Bubbling

**Event Capturing**: The event starts from the root and flows down to the target element.

**Event Bubbling**: The event starts from the target element and bubbles up to the root.

Example:

```javascript
document.getElementById("parent").addEventListener("click", () => {
  alert("Parent clicked!");
}, true);  // Capturing

document.getElementById("child").addEventListener("click", () => {
  alert("Child clicked!");
}, false);  // Bubbling
```

## 6. Difference Between DOM and BOM

- **DOM (Document Object Model)**: Deals with the document, its structure, and contents.
- **BOM (Browser Object Model)**: Deals with the browser window and everything available through it, like `window`, `navigator`, `screen`.

Example:

```javascript
// DOM manipulation
document.getElementById("myDiv").innerText = "Hello, World!";

// BOM usage
console.log(window.location.href);  // Outputs current URL
```

## 7. How to Create a Branch and Switch to That Branch in Git

To create and switch to a new branch in Git:

```bash
# Create a new branch named "new-feature"
git branch new-feature

# Switch to the new branch
git checkout new-feature

# Alternatively, create and switch in one command
git checkout -b new-feature
```

## 8. innerHTML and innerText

- **innerHTML**: Represents the HTML content (including tags).
- **innerText**: Represents the text content of an element.

Example:

```html
<div id="exampleDiv">
  <p>Hello <em>World</em></p>
</div>

<script>
  console.log(document.getElementById('exampleDiv').innerHTML);  // Outputs: <p>Hello <em>World</em></p>
  console.log(document.getElementById('exampleDiv').innerText);  // Outputs: Hello World
</script>
```

## 9. What is an Event in DOM?

An **event** in the DOM is any action that can be detected by JavaScript, such as a click, hover, key press, etc.

Example:

```javascript
document.getElementById("myButton").addEventListener("click", () => {
  alert("Button was clicked!");
});
```

## 10. Difference Between Visibility Hidden and Display None

- **visibility: hidden**: Hides the element, but it still takes up space in the layout.
- **display: none**: Removes the element from the document layout; it doesn't take up space.
