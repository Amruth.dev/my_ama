# AMA paper.

## 1. The `finally()` Method in JavaScript

### Definition and Use

The `finally()` method is used in JavaScript to specify a block of code that will be executed regardless of whether an error occurs in the preceding `try` or `catch` blocks.

### Example

```javascript
try {
    console.log("Trying to execute some code...");
    throw new Error("An error occurred!");
} catch (error) {
    console.log("Caught an error:", error.message);
} finally {
    console.log("This will always run.");
}
```

### Explanation

In the example above, the message "This will always run." is logged to the console regardless of whether the error is thrown or not.

## 2. Weakly Typed Languages

### Definition

A weakly typed (or loosely typed) language is one where variables are not bound to a specific data type, allowing more flexibility in how data is managed and manipulated.

### Example in JavaScript

```javascript
let data = 10;       // Initially a number
data = "Ten";        // Now a string
data = [10, "Ten"];  // Now an array
console.log(data);   // Output: [10, "Ten"]
```

### Explanation

In JavaScript, a variable can change its data type during execution, demonstrating the weakly typed nature of the language.

## 3. Removing Elements from a List in Python

### Problematic Example

```python
lst = [10, 20, 30, 40]
for i in lst:
    lst.remove(i)
print(lst)  # Output: [20, 40]
```

### Explanation

Removing elements from a list while iterating over it can lead to unexpected results because the list indices shift as elements are removed. A better approach is to iterate over a copy of the list or use list comprehension.

### Corrected Example

```python
lst = [10, 20, 30, 40]
lst = [item for item in lst if item != 20]  # Remove elements conditionally
print(lst)  # Output: [10, 30, 40]
```

## 4. Difference Between Alert Box and Confirm Box in JavaScript

### Alert Box

An alert box displays a message to the user and only has an "OK" button.

```javascript
alert("This is an alert box!");
```

### Confirm Box

A confirm box displays a message and has "OK" and "Cancel" buttons, returning `true` if "OK" is clicked, and `false` otherwise.

```javascript
if (confirm("Do you want to proceed?")) {
    console.log("User wants to proceed.");
} else {
    console.log("User cancelled.");
}
```

## 5. Behind the Scenes of Async Functions in JavaScript

### Explanation

When an `async` function is called, it returns a Promise. The `await` keyword inside an `async` function makes JavaScript wait for the Promise to resolve or reject before moving on to the next line of code.

### Example

```javascript
async function fetchData() {
    let response = await fetch('https://api.example.com/data');
    let data = await response.json();
    console.log(data);
}
fetchData();
```

## 6. Undefined vs Undeclared in JavaScript

### Undefined

A variable that has been declared but not assigned a value.

```javascript
let x;
console.log(x);  // Output: undefined
```

### Undeclared

A variable that has not been declared at all.

```javascript
console.log(y);  // ReferenceError: y is not defined
```

## 7. Functools in Python

### Definition

`functools` is a module in Python that provides higher-order functions to handle functions and callable objects.

### Example

```python
from functools import partial

def multiply(x, y):
    return x * y

double = partial(multiply, 2)
print(double(5))  # Output: 10
```

## 8. Slice in JavaScript

### Definition

The `slice()` method returns a shallow copy of a portion of an array into a new array object.

### Example

```javascript
let array = [1, 2, 3, 4, 5];
let slicedArray = array.slice(1, 3);
console.log(slicedArray);  // Output: [2, 3]
```

## 9. Immediately Invoked Function Expression (IIFE) in JavaScript

### Definition

An IIFE is a function that runs as soon as it is defined.

### Example

```javascript
(function() {
    console.log("IIFE executed!");
})();
```

### Explanation

The function is defined and immediately executed.

## 10. Template Literals in JavaScript

### Definition

Template literals are string literals allowing embedded expressions, which can contain placeholders indicated by the `${}` syntax.

### Example

```javascript
let name = "John";
let message = `Hello, ${name}!`;
console.log(message);  // Output: Hello, John!
```
