# Python Concepts Explained

## Iterators in Python

Iterators are objects in Python that allow you to traverse through all the elements of a collection. They implement the Iterator protocol, consisting of the `__iter__()` and `__next__()` methods. Here's a simple example:

```python
my_list = [1, 2, 3, 4, 5]
my_iter = iter(my_list)

print(next(my_iter))  # Output: 1
print(next(my_iter))  # Output: 2
```

## Inheritance

Inheritance is a feature of object-oriented programming in Python where a class can inherit attributes and methods from another class. It allows code reuse and promotes a hierarchical structure.
Example:

```python
class Animal:
    def sound(self):
        pass

class Dog(Animal):
    def sound(self):
        return "Woof!"

dog = Dog()
print(dog.sound())  # Output: Woof!
```

## Block, Inline, and Inline-Block Elements

In HTML and CSS, block elements take up the full width available, inline elements only take up as much width as necessary, and inline-block elements behave like inline elements but can have specified width and height.
Example:

```
<div style="display: block;">Block Element</div>
<span style="display: inline;">Inline Element</span>
<span style="display: inline-block; width: 100px; height: 50px; background-color: blue;">Inline-Block Element</span>
```

## self in Python

self is a reference to the current instance of a class in Python. It is used to access variables and methods within the class. Example:

```python
class MyClass:
    def __init__(self, x):
        self.x = x

    def print_x(self):
        print(self.x)

obj = MyClass(5)
obj.print_x()  # Output: 5
```

## Deleting a table

To delete a table we use

```
DROP table table_name;
```

## Functions in Python

A function is a block of reusable code that performs a specific task. It helps in modularizing code and promoting reusability. Here's how you create a user-defined function in Python:

```python
def greet(name):
    return f"Hello, {name}!"

print(greet("Alice"))  # Output: Hello, Alice!
```

## Lambda Functions in Python

Lambda functions, also known as anonymous functions, are small, inline functions defined using the lambda keyword. They are useful for short, one-line functions. Example:

```python
add = lambda x, y: x + y
print(add(2, 3))  # Output: 5
```

## Git Command for Working Directory and Staging Area Info

The git status command provides information about the working directory and staging area in Git.

## Docstring in Python

A docstring is a string literal that occurs as the first statement in a module, function, class, or method definition. It is used to document what the code does. Example:

```python
def square(x):
    """ Returns the square of a number."""
    return x * x

print(square.__doc__)  # Output: Returns the square of a number.
```

## CAP Theorem

CAP theorem states that in a distributed computer system, you can only guarantee two out of three properties: Consistency, Availability, and Partition tolerance. It's essential in designing distributed systems.
